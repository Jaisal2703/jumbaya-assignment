const express = require('express');

const app = express();

const inputOneValues = [];
const inputTwoValues = [];
const output1 = [];
const output2 = [];


app.get('/input1/:value', (req, res) => {
    const input = req.params.value;
    inputOneValues.push(input);
    if (!output1.length) {
        output1.push(input);
        if (inputTwoValues[inputOneValues.length]) {
            output1.push(inputTwoValues[inputOneValues.length]);
        }
    }
    else if(output1.length % 2 === 0) {
        if (inputOneValues[output1.length]) {
            output1.push(inputOneValues[output1.length]);
            if (inputTwoValues[output1.length]) {
                output1.push(inputTwoValues[output1.length]);
            }
        }
    }

    if (output2.length % 2 !== 0) {
        output2.push(input);
        if (inputTwoValues[inputOneValues.length]) {
            output2.push(inputTwoValues[inputOneValues.length]);
        }
    }
    console.log(inputOneValues);
    res.send(JSON.stringify({
        input1: inputOneValues,
        input2: inputTwoValues,
        output1,
        output2,
    }));
});

app.get('/input2/:value', (req, res) => {
    const input = req.params.value;
    inputTwoValues.push(input);
    if (output1.length === 1 && inputTwoValues.length === 2) {
        output1.push(input);
        if (inputOneValues[inputTwoValues.length + 1]) {
            output1.push(inputOneValues[inputTwoValues.length + 1]);
        }
    }
    else if (output1.length % 2 !==0) {
        if (inputTwoValues[output1.length]) {
            output1.push(inputTwoValues[output1.length]);
            if (inputOneValues[output1.length - 1]) {
                output1.push(inputOneValues[output1.length - 1]);
            }
        }
    }

    if (output2.length % 2 === 0) {
        output2.push(input);
        if (inputOneValues[inputTwoValues.length - 1]) {
            output2.push(inputOneValues[inputTwoValues.length - 1]);
        }
    }
    console.log(inputTwoValues);
    res.send(JSON.stringify({
        input1: inputOneValues,
        input2: inputTwoValues,
        output1,
        output2,
    }));
});

app.listen(8080, () => console.log('App is running on 8080'));

